#!/bin/bash
cd src/
HOME=/home/$USER
mkdir $HOME/.config
vim() { 
  cp -r vim $HOME/.vim
}
bash() {
  mv $HOME/.bashrc $HOME/.bashrc.old
  cat bashrc > $HOME/.bashrc 
}
neofetch() { 
  cp -r neofetch $HOME/.config/ 
} 
terminator() {
  cp -r terminator $HOME/.config/ 
}
wallpapers() { 
  PICTURES_DIR=$(grep PICTURES ~/.config/user-dirs.dirs | cut -d '=' -f2 | sed 's/"//g; s/$HOME\///')
  cp -r wallpapers/*.{jpg,png} $HOME/$PICTURES_DIR/ 
}
firefox() {
  cp -r $HOME/.mozilla/firefox $HOME/firefox.back
  rm -rf $HOME/.mozilla
  cp -r mozilla $HOME/.mozilla
  mv $HOME/firefox.back $HOME/.mozilla/
}
main() {
  vim
  bash
  neofetch
  terminator
  wallpapers
  firefox
}
main
