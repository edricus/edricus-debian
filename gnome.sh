#!/bin/bash
icons() {
  mkdir /home/$USER/.icons
  git clone https://github.com/vinceliuice/Qogir-icon-theme src/tmp/Qogir-icon-theme
  (cd src/tmp/Qogir-icon-theme ; ./install.sh)
}
gtk() {
  git clone https://github.com/vinceliuice/Qogir-theme src/tmp/Qogir-theme
  (cd src/tmp/Qogir-theme ; ./install.sh)
}
fonts() {
  mkdir -p /home/$USER/.local/share/fonts
  cp -r src/fonts/* /home/$USER/.local/share/fonts/
  fc-cache
}
gsettings_configure() {
  PICTURES_DIR=$(grep PICTURES ~/.config/user-dirs.dirs | cut -d '=' -f2 | sed 's/"//g; s/$HOME\///')/
  gsettings set org.gnome.desktop.wm.preferences button-layout appmenu:minimize,maximize,close
  gsettings set org.gnome.desktop.interface gtk-theme 'Qogir-dark'
  gsettings set org.gnome.desktop.interface icon-theme 'Qogir-manjaro-dark'
  gsettings set org.gnome.shell.extensions.user-theme name 'Qogir-dark'
  gsettings set org.gnome.desktop.interface cursor-theme 'DMZ-White'
  gsettings set org.gnome.desktop.background picture-uri "file:///home/$USER/$PICTURES_DIR/nord-1.png"
  gsettings set org.gnome.mutter dynamic-workspaces false
  gsettings set org.gnome.desktop.wm.preferences num-workspaces 5
  gsettings set org.gnome.desktop.interface clock-show-date true
  gsettings set org.gnome.shell favorite-apps "['firefox-esr.desktop', 'org.gnome.Nautilus.desktop', 'terminator.desktop', 'discord.desktop', 'telegramdesktop.desktop', 'org.gnome.Software.desktop']"
  gsettings set org.gnome.desktop.interface monospace-font-name "UbuntuMono Nerd Font Regular 15"
  gsettings set org.gnome.shell enabled-extensions "['auto-move-windows@gnome-shell-extensions.gcampax.github.com', 'user-theme@gnome-shell-extensions.gcampax.github.com']"
}
main() {
  icons
  gtk
  fonts
  gsettings_configure
}
main
