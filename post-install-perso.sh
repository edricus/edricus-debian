#!/bin/bash -e

### Conditions ###

if [ "$(id -u)" != "0" ]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi
if [[ -z "$(grep Debian /etc/os-release)" ]]; then
  echo "This script is intended for Debian"
  exit
fi

### Write package list ###

write_softwares() {
  SOFTWARE="$(whiptail --title "Choose additionnal software" --backtitle "Installing software" --checklist --separate-output 3>&1 1>&2 2>&3 \
    "Check to install" 15 80 7 \
    "flatpak" "Application deployment framework for desktop apps" ON \
    "timeshift" "System restore utility" ON \
    "telegram-desktop" "Fast and secure messaging application" OFF \
    "gimp" "GNU Image Manipulation Program" OFF )"
  printf "${SOFTWARE}%1s" >> ${PACKAGELIST}
}
write_thirdparty() {
  THIRDPARTY="$(whiptail --title "Choose 3rd party software" --backtitle "Installing software" --checklist --separate-output 3>&1 1>&2 2>&3 \
    "Check to install" 15 80 7 \
    "libreoffice" "Office productivity suite" ON \
    "virtualbox-6.1" "Oracle virtualization solution" ON \
    "signal-desktop" "Private messenger" OFF \
    "discord" "Messaging, Voice, and Video Client" OFF \
    "element-desktop" "Matrix-based end-to-end encrypted messenger" OFF )"
  printf "${THIRDPARTY}%1s" >> ${PACKAGELIST}
}
write_manual() {
  MANUAL="$(whiptail --title "Additionnal software" --backtitle "Installing software" --inputbox --separate-output 3>&1 1>&2 2>&3 \
    "Enter additionnal software bellow (separated with space, leave blank for nothing)" 15 80 )"
  printf "${MANUAL}%1s" >> ${PACKAGELIST}
}

### Prepare packages ###

libreoffice() {
  if [[ -n $(grep -o libreoffice ${PACKAGELIST}) ]]; then
    echo 'libreoffice-l10n-fr' >> ${PACKAGELIST}
  fi
}
virtualbox() {
  if [[ -n $(grep -o virtualbox ${PACKAGELIST}) ]]; then
    wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | apt-key add -
    echo "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian bullseye contrib" > /etc/apt/sources.list.d/virtualbox.list
  fi
}
discord() {
  if [[ -n $(grep -o discord ${PACKAGELIST}) ]]; then
    cd ${TMPDIRECTORY}
    wget -O discord.tar.gz "https://discordapp.com/api/download?platform=linux&format=tar.gz"
    tar -xf discord.tar.gz
    desktop-file-validate Discord/discord.desktop
    mv Discord/discord.desktop /usr/share/applications/
    mv Discord /usr/share/
    cd -
    ln -s /usr/share/Discord/Discord /usr/bin/discord
    sed -i s/discord// ${PACKAGELIST}
  fi
}
signal() {
  if [[ -n $(grep -o signal-desktop ${PACKAGELIST}) ]]; then
    echo 'deb [signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg arch=amd64] https://updates.signal.org/desktop/apt xenial main' > /etc/apt/sources.list.d/signal.list
    wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > ${TMPDIRECTORY}/signal-desktop-keyring.gpg
    cat ${TMPDIRECTORY}/signal-desktop-keyring.gpg > /usr/share/keyrings/signal-desktop-keyring.gpg
  fi
}
element() {
  if [[ -n $(grep -o element-desktop ${PACKAGELIST}) ]]; then
    echo 'deb [signed-by=/usr/share/keyrings/riot-im-archive-keyring.gpg] https://packages.riot.im/debian/ default main' > /etc/apt/sources.list.d/riot-im.list
    wget -O /usr/share/keyrings/riot-im-archive-keyring.gpg https://packages.riot.im/debian/riot-im-archive-keyring.gpg	
  fi
}

### Install ###

install_base() {
  dpkg --add-architecture i386
  export DEBIAN_FRONTEND=noninteractive
  apt-get update 
  apt-get install -y \
    git curl wget vim-gtk3 ssh htop dbus lm-sensors neofetch \
    apt-transport-https ca-certificates gnupg2 software-properties-common sassc linux-headers-amd64 \
    gnome-tweaks terminator timeshift dmz-cursor-theme gnome-software-plugin-flatpak gparted grub-customizer
}

install_list() {
  for i in $(awk '{print $0" "}' ${PACKAGELIST} | tr -d '\n'); do
  apt-get install -y $i
  done
}

remove_bloats() { 
  apt-get purge --autoremove -y gnome-terminal gnome-games 
}
### Configure ###

postinstall_flatpak() {
  if [[ -n $(dpkg-query -l | grep -Eo "ii.*flatpak ") ]]; then
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
  fi
}
postinstall_virtualbox() {
  if [[ -n $(dpkg-query -l | grep -Eo "ii.*virtualbox-6.1 ") ]]; then
    wget "https://download.virtualbox.org/virtualbox/6.1.26/Oracle_VM_VirtualBox_Extension_Pack-6.1.26.vbox-extpack" -P ${TMPDIRECTORY}/
    yes | vboxmanage extpack install ${TMPDIRECTORY}/*.vbox-extpack
  fi
}
configure_gtk() {
su -c ./gnome.sh $USERID
}
misc() { 
  su -c ./misc.sh $USERID 
}

### Run ###

write_list() {
  PACKAGELIST=$(mktemp)
  write_softwares
  write_thirdparty
  write_manual
}
prepare() {
  TMPDIRECTORY=$(mktemp -d) 
  libreoffice
  virtualbox
  discord
  signal
  element
}
install() {
  install_base
  install_list
  remove_bloats
}
configure() {
  postinstall_flatpak
# postinstall_virtualbox # VERR_VERSION_MISMATCH
  USERID=$(getent passwd $SUDO_UID | cut -d: -f1) # get user id to run non-sudo parts
  configure_gtk
  misc
}

main() {
  write_list
  prepare
  install
  configure
}
main
echo "Done - reboot to apply"
